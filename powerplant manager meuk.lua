-- -- This is intended to charge batteries and disconnect them from the grid once they are fully charged.
-- -- It is intended to keep power at hand for restarting a power plant.
-- -- Connect network cables to a battery labeled "emergency battery" and a switch labeled "emergensy batterySwitch".

-- local pollInterval <const> = 5  -- How often to check on battery charge and grid power in seconds. Don't set this to 0 or your framerate will collapse.
-- local category <const> = "powerPlant"  -- The label identifying which switch and battery are our concern.

-- local MODE <const> = { NORMAL="1", MANUAL="2", CHARGING="3" }

-- local battery = component.proxy(component.findComponent(category .. " battery")[1])

-- local batterySwitch = component.proxy(component.findComponent(category .. " batterySwitch")[1])
-- local gridSwitch = component.proxy(component.findComponent(category .. " gridSwitch")[1])

-- local batteryConnectors = batterySwitch:getPowerConnectors()
-- local gridConnectors = gridSwitch:getPowerConnectors()

-- local indicatorPole = component.proxy(component.findComponent(findClass("IndicatorPole_C"))[1]) or nil
-- local isUtilityFusePopped = false

-- local generators = component.proxy(component.findComponent(findClass("FGBuildableGenerator")))

-- function setStandby(buildables, standby)
--     for key, generator in pairs(buildables) do
--         generator.standby = standby
--     end
-- end

-- function flipBatterySwitch(newState)
--     batterySwitch.isSwitchOn = newState
-- end

-- function isFilled(generator)
--     local inventories = generator:getInventories()

--     -- for i, inventory in pairs(inventories) do
--     for i = 1, #inventories - 1 do -- for some reason the last inventory is bogus
--         local inventory = inventories[i]
--         for j = 0, inventory.size - 1 do
--             local stack = inventory:getStack(j)
--             if stack == nil then
--                 goto continueJ
--             end

--             local item = stack.item
--             if item.type == nil then
--                 return false
--             end

--             if stack.count ~= item.type.max then
--                 return false
--             end

--             ::continueJ::
--         end
--     end

--     return true
-- end

-- function setIndicator(level)
--     if level == 1 then
--         -- green
--         indicatorPole:setColor(0, 1, 0, 1)        
--     elseif level == 2 then
--         -- yellow
--         indicatorPole:setColor(1, 1, 0, 1)
--     elseif level == 3 then
--         -- red
--         indicatorPole:setColor(1, 0, 0, 1)
--     else
--         -- off
--         indicatorPole:setColor(0, 0, 0, 0)
--     end
-- end


-- function startPlant()

--   print("Starting generators")

--   -- Turn off all generators
--   setStandby(generators, true)

--   -- Start fuel production
--   -- TODO: in een functie stoppen die kijkt of er genoeg power is ergens
--   flipBatterySwitch(true)

--   -- Wait for a generator to be filled with fuel and water
--   local nrGeneratorsOn = 0
--   while true do
--     for key, generator in pairs(generators) do

--         print("standby, filled:", generator.standby, isFilled(generator))
--         if (generator.standby and isFilled(generator)) then
--             nrGeneratorsOn = nrGeneratorsOn + 1
   
--             print("Turning on generator (" .. nrGeneratorsOn .. "/" .. #generators .. ")")
--             generator.standby = false
--         end
        



--     end

--     event.pull(pollInterval)
--   end


  

-- end

-- -- function monitorFuse()
-- --     if not isUtilityFusePopped and batteryCircuitUtilitySide.isFuesed then
-- --         print("The utility fuse has popped, please reset it")

-- --         setIndicator(3)

-- --         isUtilityFusePopped = true
-- --     elseif isUtilityFusePopped and not batteryCircuitUtilitySide.isFuesed then
-- --         print("The utility fuse has been reset")

-- --         setIndicator(1) -- todo: meer centraal regelen
        
-- --         isUtilityFusePopped = false
-- --     end
-- -- end


-- print("Found generators:")
-- for key, generator in pairs(generators) do
--   print(key, generator)
-- end


--print("fuelPlants", fuelPlants)


function startPlant2()
    while true do
        print("starting plant")

        coroutine.yield()
    end
end


function managePower()
    while true do
        print("managing power")

        coroutine.yield()
    end
end



thread1 = coroutine.create(startPlant2)
thread2 = coroutine.create(managePower)

while true do
    coroutine.resume(thread1)
    coroutine.resume(thread2)

    event.pull(5)
end

-- startPlant()

