local pollInterval <const> = 5
local category <const> = "plantSwitch"

local MODE <const> = { NORMAL="1", POWERED_DOWN="2", FUSE_POPPED="3" }

local mode = MODE.POWERED_DOWN

-- TODO: only get the poles labled <category>
local indicatorPoles = component.proxy(component.findComponent(findClass("IndicatorPole_C")))

local gridSwitch = component.proxy(component.findComponent(category .. " switch")[1])
local connectors = gridSwitch:getPowerConnectors()


function setIndicators(level)
  local COLORS <const> = {
    {0, 1, 0, 1}, -- green
    {1, 1, 0, 1}, -- yellow
    {1, 0, 0, 1}, -- red
  }

  color = COLORS[level]
  if color == nil then
    color = {0, 0, 0, 0}
  end

  for i, indicatorPole in pairs(indicatorPoles) do
    indicatorPole:setColor(table.unpack(color))
  end
end

function getProduction(circuit)
  return (circuit and circuit ~= nil and circuit.production) or 0
end

function getConsumption(circuit)
  return (circuit and circuit ~= nil and circuit.consumption) or 0
end


function getSurplus(circuit)
  production = getProduction(circuit)
  consumption = getConsumption(circuit)
  surplus = production - consumption

  print("production, consumption, surplus:", production, consumption, surplus)

  return surplus
end

function hasTrippedFuse(circuits)
  for key, circuit in pairs(circuits) do
    if circuit.isFuesed then
      return true
    end
  end

  return false
end

function setMode(newMode)
  if mode == newMode then
    return
  end

  if mode == MODE.FUSE_POPPED then
    print("A fuse has been reset")
  end

  mode = newMode

  if mode == MODE.NORMAL then
    setIndicators(1)
    print("Factory powered up")
  elseif mode == MODE.POWERED_DOWN then
    setIndicators(2)
    print("Factory powered down")
  elseif mode == MODE.FUSE_POPPED then
    setIndicators(3)
    print("A fuse has popped, please reset it")
  else
    setIndicators(nil)
  end
end

function determineMaxLoad()
  local originalGridSwitchPosition = gridSwitch.isSwitchOn
  gridSwitch.isSwitchOn = false
  event.pull(0.5) -- we cannot get the new values right away

  maxLoad = (factoryCircuit and factoryCircuit.maxPowerConsumption) or 0
  gridSwitch.isSwitchOn = originalGridSwitchPosition
end

function bootUp()
  print("Starting")

  setIndicators(3) -- we don't know where we stand so in case of a crash, level 3 is best
  gridSwitch.isSwitchOn = false

  findGridCircuit()
  determineMaxLoad()
end

function findGridCircuit()
  local circuits = { connectors[1]:getCircuit(), connectors[2]:getCircuit() }

  print("Finding circuits...")
  if getProduction(circuits[1]) > 1 then
    gridCircuit, factoryCircuit = circuits[1], circuits[2]
    return
  elseif getProduction(circuits[2]) > 1 then
    gridCircuit, factoryCircuit = circuits[2], circuits[1]
    return
  end

  print("Waiting for power...")
  event.pull(5)
end


bootUp()

while true do
  -- Determine if the grid can take our load
  print("gridcircuit", gridCircuit)
  gridSurplus = getSurplus(gridCircuit)
  print("isSwitchOn, trippedFuse, gridProduction, gridSurplus, maxLoad:", gridSwitch.isSwitchOn, hasTrippedFuse({gridCircuit, factoryCircuit}), getProduction(gridCircuit), gridSurplus, maxLoad)

  if gridSwitch.isSwitchOn == false then
     -- the factory might have changed since booting
    findGridCircuit()
    determineMaxLoad()

    if not hasTrippedFuse({gridCircuit, factoryCircuit}) and gridSurplus > maxLoad then
      setMode(MODE.NORMAL)
      gridSwitch.isSwitchOn = true
    end
  else
    if hasTrippedFuse({gridCircuit, factoryCircuit}) then
      setMode(MODE.FUSE_POPPED)
      gridSwitch.isSwitchOn = false
    elseif gridSurplus <= 0 then
      setMode(MODE.POWERED_DOWN)
      gridSwitch.isSwitchOn = false
    end
  end

  event.pull(pollInterval)
end
