-- -- This is intended to charge batteries and disconnect them from the grid once they are fully charged.
-- -- It is intended to keep power at hand for restarting a power plant.
-- -- Connect network cables to a battery labeled "emergency battery" and a switch labeled "emergensy batterySwitch".

local pollInterval <const> = 5  -- How often to check on battery charge and grid power in seconds. Don't set this to 0 or your framerate will collapse.
local category <const> = "powerPlant"  -- The label identifying which switch and battery are our concern.

local PRODUCTION_MODE <const> = { OFF="1", GRID_START="2", BATTERY_START="3", UNPOWERED_START="4", OPERATIONAL="5" }
local SUPPLY_MODE <const> = { GRID_DOWN="1", FUSE_BLOWN="2", FUSES_BLOWN="3", GRID_CONNECTED="4" }

local productionMode = nil
local supplyMode = nil

local battery = component.proxy(component.findComponent(category .. " battery")[1])

local batterySwitch = component.proxy(component.findComponent(category .. " batterySwitch")[1])
local gridSwitch = component.proxy(component.findComponent(category .. " gridSwitch")[1])

local batteryConnectors = batterySwitch:getPowerConnectors()
local gridConnectors = gridSwitch:getPowerConnectors()

-- local indicatorPole = component.proxy(component.findComponent(findClass("IndicatorPole_C"))[1]) or nil
-- local isUtilityFusePopped = false

local generators = component.proxy(component.findComponent(findClass("FGBuildableGenerator")))


-- function hasTrippedFuse(circuits)
--     for key, circuit in pairs(circuits) do
--         if circuit.isFuesed then
--             return true
--         end
--     end
  
--     return false
-- end

function getSurplus(circuit)
    production = getProduction(circuit)
    consumption = circuit.consumption or 0
    surplus = production - consumption
  
    print("production, consumption, surplus:", production, consumption, surplus)
  
    return gridSurplus
end

function canConnectToGrid()
    if hasTrippedFuse() then


    end

end


function canChargeBattery()


end


function canDischargeBattery()


end







-- Starts generators
function managePlant()
    while true do
        print("starting plant")

        coroutine.yield()
    end
end


-- Controls the power switches for grid and battery
function managePower()
    while true do
        local newState = nil
        print("managing power")

        local gridSurplus = getSurplus(gridCircuit)
        local plantSurplus = getSurplus(plantCircuit)
        if productionMode == PRODUCTION_MODE.OPERATIONAL then
            
            
        else


        









        ::endIteration::

        coroutine.yield()
    end
end



plantThread = coroutine.create(managePlant)
powerThread = coroutine.create(managePower)

while true do
    coroutine.resume(powerThread)
    coroutine.resume(plantThread)

    event.pull(5)
end
