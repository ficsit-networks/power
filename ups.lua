local pollInterval <const> = 5
local category <const> = "UPS"

local MODE <const> = { NORMAL="1", DISCHARGING="2", CHARGING="3", OFF="4", TRIPPED_FUSE="5" }

local battery = component.proxy(component.findComponent(category .. " battery")[1])
local batterySwitch = component.proxy(component.findComponent(category .. " batterySwitch")[1])
local mainsSwitch = component.proxy(component.findComponent(category .. " mainsSwitch")[1])
local connectors = mainsSwitch:getPowerConnectors()
local circuit1, circuit2, currMode


function hasTrippedFuse(circuits)
  for key, circuit in pairs(circuits) do
      if circuit.isFuesed then
          return true
      end
  end

  return false
end

function getGridSurplus(gridCircuit)
  gridProduction = (gridCircuit and gridCircuit.production) or 0
  gridConsumption = gridCircuit.consumption or 0
  gridSurplus = gridProduction - gridConsumption

  print("production, consumption, surplus:", gridProduction, gridConsumption, gridSurplus)

  return gridSurplus
end

-- Set switches to default settings and then enter loop to figure out where we stand
batterySwitch:setIsSwitchOn( false )
mainsSwitch:setIsSwitchOn( true )

while( true ) do

  -- We get the circuits inside the loop so that we can see a wire being attached
  circuit1, circuit2 = connectors[1]:getCircuit(), connectors[2]:getCircuit()

  -- Determine if the grid has power
  if circuit1 and circuit1.production > 0 then
    gridCircuit = circuit1
  else
    gridCircuit = circuit2
  end

  -- Determine what to do
  if  gridCircuit.production > 0 then
    if battery.powerStore == 100 and not hasTrippedFuse({circuit1, circuit2}) then

      -- We got mains power and the battery doesn't need to charge, all is well
      if currMode ~= MODE.NORMAL then
        print("Switching to normal mode")
        batterySwitch:setIsSwitchOn( false )
        mainsSwitch:setIsSwitchOn( true )
        currMode = MODE.NORMAL
      end
    elseif
      not hasTrippedFuse({circuit1, circuit2})
      and getGridSurplus(gridCircuit) > 0 -- Make sure we won't accidentally discharge the battery
    then
      -- We got mains power; charge battery
      if currMode ~= MODE.CHARGING then
        print("Charging battery")
        mainsSwitch:setIsSwitchOn( true )
        batterySwitch:setIsSwitchOn( true )
        currMode = MODE.CHARGING
      end
      print("Battery charge", battery.powerStore, "MWh")
    end
  else -- gridCircuit.production == 0
    -- Mains power down; run on battery
    if currMode ~= MODE.DISCHARGING then
      print("Running on battery")
      currMode = MODE.DISCHARGING
    end
    mainsSwitch:setIsSwitchOn( false )
    batterySwitch:setIsSwitchOn( true )
    print("Battery charge", battery.powerStore, "MWh")
  end

  event.pull(pollInterval)
end
